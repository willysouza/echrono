import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the GeralPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/geral/geral.html',
})
export class GeralPage {

  constructor(private nav: NavController) {

  }

}
