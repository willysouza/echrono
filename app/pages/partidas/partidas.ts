import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DAOPartidas } from '../../dao/dao-partidas';
import { PeopleService } from '../../providers/people-service/people-service';

/*
  Generated class for the PartidasPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/partidas/partidas.html',
  providers: [PeopleService]
})
export class PartidasPage {
  public people: any;

constructor(private nav: NavController, public peopleService: PeopleService) {
  this.loadPeople();
}

  loadPeople(){
    this.peopleService.load()
    .then(data => {
      this.people = data;
    });
  }
}
