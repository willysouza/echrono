import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the ParceriasPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/parcerias/parcerias.html',
})
export class ParceriasPage {

  constructor(private nav: NavController) {

  }

}
