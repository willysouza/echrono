import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';


/*
  Generated class for the ModalPalestraPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/modal-palestra/modal-palestra.html',

})
export class ModalPalestraPage {
    public people: any;
  constructor(params: NavParams, private nav: NavController, public view: ViewController) {
    this.people = params.get('person');
  }

    close(){
      this.view.dismiss();
    }
}
