export class DAOPartidas{
  partidaslol: any;
  partidasdota: any;
  constructor(){
    this.partidaslol = [];
    this.partidasdota = [];
  }

  getList(){
    this.partidaslol = [
      {horario: "10:00",
       time1: "Time 1a",
       time2: "Time 2a",
       local: "Lab 2"},
      {horario: "11:00",
        time1: "Time 1b",
        time2: "Time 2b",
       local: "Lab 3"},
      {horario: "12:00",
        time1: "Time 1c",
        time2: "Time 2c",
       local: "Lab 4"}
    ];
    return this.partidaslol;
  }

  getList1(){
    this.partidasdota = [
      {horario: "10:30",
       time1: "Time A1",
       time2: "Time B1",
       local: "Lab 2"},
      {horario: "11:30",
        time1: "Time A2",
        time2: "Time B2",
       local: "Lab 3"},
      {horario: "12:30",
        time1: "Time A3",
        time2: "Time B3",
       local: "Lab 4"}
    ];
    return this.partidasdota;
  }
}
