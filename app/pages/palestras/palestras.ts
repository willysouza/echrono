import { Component } from '@angular/core';
import { NavController, Modal } from 'ionic-angular';
import {DAOPalestras} from "../../dao/dao-palestras";
import { PeopleService } from '../../providers/people-service/people-service';
import { ModalPalestraPage } from '../modal-palestra/modal-palestra';
/*
  Generated class for the PalestrasPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/palestras/palestras.html',
  providers: [PeopleService]
})
export class PalestrasPage {
      public people: any;

  constructor(private nav: NavController, public peopleService: PeopleService) {
      this.loadPeople();
  }

      openModal(person){
        let modal = Modal.create(ModalPalestraPage, {person});
        this.nav.present(modal);
      }


      loadPeople(){
        this.peopleService.load()
        .then(data => {
          this.people = data;
        });
      }
}
