import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

/*
  Generated class for the ModalMinicursoPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/modal-minicurso/modal-minicurso.html',
})
export class ModalMinicursoPage {
  public people: any;
  constructor(params: NavParams, private nav: NavController, public view: ViewController) {
  this.people = params.get('person');
  }

  close(){
    this.view.dismiss();
  }
}
