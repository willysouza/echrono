export class DAOPalestras{
  listPalestras: any;
  constructor(){
    this.listPalestras = [];
  }

  getList(){
    this.listPalestras = [
      {data: "Dia 1",
       horario: "12:00",
       local: "LocalTeste",
       palestrante:"PalestranteTeste"},
      {data: "Dia 2",
       horario: "13:00",
       local: "LocalTeste2",
       palestrante:"PalestranteTeste2"}
    ];
    return this.listPalestras;
  }

}
