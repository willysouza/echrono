import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {GeralPage} from '../geral/geral';
import {PartidasPage} from '../partidas/partidas';
import {PalestrasPage} from '../palestras/palestras';
import {FavoritosPage} from '../favoritos/favoritos';
import {MiniCursosPage} from '../mini-cursos/mini-cursos';
import {SobrePage} from '../sobre/sobre';
import {ParceriasPage} from '../parcerias/parcerias';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {

  partidas: any = PartidasPage;
  palestras: any = PalestrasPage;
  geral: any = GeralPage;
  miniCursos: any = MiniCursosPage;
  sobre: any = SobrePage;
  parcerias: any = ParceriasPage;

  constructor(private navCtrl: NavController) {

  }

    pushPage(x){
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.navCtrl.push(x);
  };
}
