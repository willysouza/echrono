import {Component, ViewChild} from '@angular/core';
import {Platform, ionicBootstrap, Nav} from 'ionic-angular';
import {StatusBar} from 'ionic-native';

import {HomePage} from './pages/home/home';
import {PartidasPage} from './pages/partidas/partidas';
import {PalestrasPage} from './pages/palestras/palestras';
import {GeralPage} from './pages/geral/geral';
import {FavoritosPage} from './pages/favoritos/favoritos';
import {MiniCursosPage} from './pages/mini-cursos/mini-cursos';
import {SobrePage} from './pages/sobre/sobre';
import {ParceriasPage} from './pages/parcerias/parcerias';

@Component({
  templateUrl:'build/app.html'
})
class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform) {

    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Favoritos', component: FavoritosPage },
      { title: 'Geral', component: GeralPage},
      { title: 'Mini Cursos', component: MiniCursosPage},
      { title: 'Partidas', component: PartidasPage},
      { title: 'Palestras', component: PalestrasPage},
      { title: 'Sobre', component: SobrePage},
      { title: 'Parcerias', component: ParceriasPage}
    ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });


  }

    openPage(page){
      this.nav.setRoot(page.component);
    };

}

ionicBootstrap(MyApp);
